﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;


namespace discord_output_bot
{
    public class DiscordBot
    {
        DiscordClient client;
        CommandService command;
        string token = "MzE5MzE4NzY5OTQxODcyNjQz.DBBE2g.sH1QSBLgK0rCbASY42KqoIMDIeY";

        public DiscordBot()
        {
            client = new DiscordClient(input =>
            {
                input.LogLevel = LogSeverity.Info;
                input.LogHandler = Log;
            });

            client.UsingCommands(input =>
            {
               input.PrefixChar = '!';
               input.AllowMentionPrefix = true;
            });

            command = client.GetService<CommandService>();

            command.CreateCommand("Hello").Do(async (e) =>
            {
                await e.Channel.SendMessage("World!");
            });

            client.MessageReceived += async (s, e) =>
            {
                if (!e.Message.IsAuthor)
                    await e.Channel.SendMessage(e.Message.Text);

                Console.WriteLine(e.Message.Text);
            };

            client.ExecuteAndWait(async () =>
            {
                await client.Connect(token, TokenType.Bot);
            });
        }

        private void Log(object sender, LogMessageEventArgs e)
        {
            Console.WriteLine(e.Message);
        }

    }
}
